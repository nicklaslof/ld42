﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class IslandBuilder : MonoBehaviour {


    public Tilemap island;
    public Tilemap decoration;
    public Tilemap obstacles;
    public Tilemap water;



    public Tile grassTile;
    public Tile sandTile;
    public Tile waterTile;

    public Tile smoothSandTileLeft;

    public Tile grassDecorationTile1;
    public Tile grassDecorationTile2;
    public Tile grassDecorationTile3;

    public Tile smallStonesTile1;
    public Tile smallStonesTile2;

    public Tile flowerTile1;
    public Tile flowerTile2;

    public Tile bigBush;
    public Tile bigStone;
    public Tile bigStone2;
    private FontRender errorRender;
    public int seed;
    public int previousSeed = -1;
    public int previousRandom;

    public int size = 100;
    public float scale;
    public float modifier;
    public float scale2;



    public float decorationScale;
    public float decorationScale2;

    public float obstacleScale;
    public float obstacleScale2;


    public bool initialGenerationDone = false;

    public Image error;




	// Use this for initialization
	void Start () {
        this.errorRender = error.GetComponent<FontRender>();
        seed = Random.Range(0, int.MaxValue/2);
        int random;
        // Should be moved to Start when done playing around with values
       // int random = previousRandom;
      //  if (seed != previousSeed) {
            Random.InitState(seed);
            //previousSeed = seed;
            random = Random.Range(1, 20000);

            bool acceptableIsland = false;
            int maxCounter = 0;

           while (!acceptableIsland) {
                maxCounter++;
                Debug.Log("Generating island");
                generateIsland(random);
                acceptableIsland = verifyIsland();
                if (!acceptableIsland) {
                    seed += 1;
                    Random.InitState(seed);
                    random = Random.Range(1, 20000);
                }
                if (maxCounter > 50){
                    break;
                }
            }

        if (!acceptableIsland) {
            errorRender.text = "SORRY COULDNT GENERATE AN ISLAND. PLEASE RESTART";
        }
        else {

            generateDecoration(random);
            generateObstacles(random);

            initialGenerationDone = true;
        }
    }

    // Update is called once per frame
	void Update () {

    }

    private bool verifyIsland(){

        int numberOfGrassTiles = 0;

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (isGrassTile(island, x, y)){
                    numberOfGrassTiles++;
                }
            }
        }

        Debug.Log(numberOfGrassTiles);
        if (numberOfGrassTiles > 1100 && numberOfGrassTiles < 1600){
            return true;
        }


        return false;
    }

    private void generateObstacles(int random){
        obstacles.ClearAllTiles();
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                int perlinX = x + random;
                int perlinY = y + random;
                float noise = Mathf.PerlinNoise(perlinX * (obstacleScale), perlinY * (obstacleScale)) * (obstacleScale2);

                if (noise > 0.65f && noise < 0.67f) {

                    if (isGrassTile(island, x, y)) {
                        obstacles.SetTile(new Vector3Int(x, y, 0), bigStone);
                    }
                }

                if (noise > 0.66f && noise < 0.68f) {

                    if (isGrassTile(island, x, y)) {
                        obstacles.SetTile(new Vector3Int(x, y, 0), bigStone2);
                    }
                }

                if (noise > 0.55f && noise < 0.58f) {

                    if (isGrassTile(island, x, y)) {
                        obstacles.SetTile(new Vector3Int(x, y, 0), bigBush);
                    }
                }



            }
        }

    }

    private void generateDecoration(int random){
        decoration.ClearAllTiles();
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                int perlinX = x + random;
                int perlinY = y + random;

                float noise = Mathf.PerlinNoise(perlinX * decorationScale , perlinY * decorationScale)* decorationScale2;
                float noise2 = Mathf.PerlinNoise(perlinX * decorationScale, perlinY * decorationScale) * decorationScale2;

                if (noise > 0.2f && noise < 0.32f) {

                    if (isGrassTile(island,x,y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), grassDecorationTile1);
                    }
                }

                if (noise > 0.32f && noise < 0.34f){
                    if (isGrassTile(island, x, y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), grassDecorationTile2);
                    }
                }

                if (noise > 0.5f && noise < 0.52f) {

                    if (isGrassTile(island, x, y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), smallStonesTile1);
                    }
                }

                if (noise > 0.35f && noise < 0.38f) {

                    if (isGrassTile(island, x, y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), flowerTile2);
                    }
                }

                if (noise > 0.4f && noise < 0.45f) {

                    if (isGrassTile(island, x, y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), smallStonesTile2);
                    }
                }
                if (noise2 > 0.6f && noise2 < 0.65f) {

                    if (isGrassTile(island, x, y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), flowerTile1);
                    }
                }



                if (noise2+noise > 0.4f && noise2+noise < 0.50f) {
                    if (isGrassTile(island, x, y)) {
                        decoration.SetTile(new Vector3Int(x, y, 0), grassDecorationTile3);
                    }
                }

            }
        }

        decoration.RefreshAllTiles();
    }

    private bool isGrassTile(Tilemap tileMap, int x, int y){
        Vector3Int pos = new Vector3Int(x, y, 0);
        if (island.HasTile(pos)) {
            if (island.GetTile(pos).Equals(grassTile)){
                return true;
            }
        }

        return false;
    }

    private bool isSandTile(Tilemap tileMap, int x, int y) {
        Vector3Int pos = new Vector3Int(x, y, 0);
        if (island.HasTile(pos)) {
            if (island.GetTile(pos).Equals(sandTile)) {
                return true;
            }
        }

        return false;
    }

    private bool isWaterTile(Tilemap tileMap, int x, int y) {
        Vector3Int pos = new Vector3Int(x, y, 0);
        if (water.HasTile(pos)) {
            if (water.GetTile(pos).Equals(waterTile)) {
                return true;
            }
        }

        return false;
    }

    private void generateIsland(int random) {
        island.ClearAllTiles();
        water.ClearAllTiles();
        int center = size / 2;

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                int perlinX = x + random;
                int perlinY = y + random;

                float noise = Mathf.PerlinNoise(perlinX * scale, perlinY * scale) * scale2;

                noise += Mathf.PerlinNoise(perlinX * modifier, perlinY * modifier) * scale2;

                float distanceX = (center - x) * (center - x);
                float distanceY = (center - y) * (center - y);

                float distanceToCenter = Mathf.Sqrt(distanceX + distanceY);

                distanceToCenter /= (float)size;

                // The farther away we are from the center the chance of water is bigger (since we are subtracting the noise value with the distance)
                noise -= (distanceToCenter * 2);

                if (noise < 0.4f) {
                    water.SetTile(new Vector3Int(x, y, 0), waterTile);
                }

                if (noise > 0.4f && noise < 0.48f) {
                    island.SetTile(new Vector3Int(x, y, 0), sandTile);
                }

                if (noise > 0.48f) {
                    island.SetTile(new Vector3Int(x, y, 0), grassTile);
                }
            }
        }

        island.RefreshAllTiles();
        previousRandom = random;
    }
}
