﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputBehaviour : MonoBehaviour {
    private Rigidbody2D body;
    private Vector2 velocityVector;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    public EndScene endScene;



    public float veloctiy;

    // Use this for initialization
	void Start () {
        this.body = GetComponent<Rigidbody2D>();
        this.velocityVector = new Vector2();

        this.spriteRenderer = GetComponent<SpriteRenderer>();
	}

	private void FixedUpdate() {

        if (!endScene.isEndScenePlaying()) {
            velocityVector.x = 0;
            velocityVector.y = 0;

            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");

            if (horizontal > 0.0f || horizontal < 0.0f) {
                this.velocityVector.x = horizontal;
            }

            if (vertical > 0.0f || vertical < 0.0f) {
                this.velocityVector.y = vertical;
            }


            if (this.velocityVector.magnitude > 1) {
                this.velocityVector.x /= 1.4f;
                this.velocityVector.y /= 1.4f;
            }

            //Debug.Log(this.velocityVector.magnitude);

            this.velocityVector *= veloctiy;





            this.body.velocity = this.velocityVector;
        }
	}

	// Update is called once per frame
	void Update () {



      
	}
}
