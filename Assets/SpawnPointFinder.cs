﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawnPointFinder : MonoBehaviour {

    public Grid gameGrid;
    public Tile startTile;
    public Tilemap island;
    private IslandBuilder islandBuilder;
    public bool spawnPointFound;


    List<Vector3Int> possibleSpawnPoints = new List<Vector3Int>();

	// Use this for initialization
	void Start () {
        this.islandBuilder = gameGrid.GetComponent<IslandBuilder>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!spawnPointFound) {
            if (this.islandBuilder.initialGenerationDone) {
                for (int x = 0; x < 100; x++) {
                    for (int y = 0; y < 100; y++) {
                        Vector3Int v = new Vector3Int(x, y, 0);
                        if (island.HasTile(v)) {
                            if (island.GetTile(v).Equals(startTile)) {
                                possibleSpawnPoints.Add(v);
                            }
                        }

                    }
                }
                int randomPosition = UnityEngine.Random.Range(0, possibleSpawnPoints.Count);

                Vector3Int position = possibleSpawnPoints[randomPosition];
                if (position != null) {
                    setSpawnPoint(position.x + 1, position.y + 1);
                    spawnPointFound = true;
                }
            }
        }
	}

    private void setSpawnPoint(int x, int y) {
        transform.position = new Vector3(x, y, 1);
    }
}
