﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAgain : MonoBehaviour {

    private float countdown;

	// Use this for initialization
	void Start () {
        countdown = 3.0f;
	}
	
	// Update is called once per frame
	void Update () {
        countdown -= Time.deltaTime;


        if (countdown < 0.0f){
            if (Input.anyKey){
                UnityEngine.SceneManagement.SceneManager.LoadScene("Intro");
            }
        }
	}
}
