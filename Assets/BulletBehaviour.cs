﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour {
    private Rigidbody2D body;
    private Vector2 bulletDirection;

    public float lifeTime = 1.0f;

    public float velocity = 1.0f;


    // Use this for initialization
	void Start () {
        this.body = GetComponent<Rigidbody2D>();
	}



	private void FixedUpdate() {
        body.velocity += bulletDirection*velocity;


	}
	// Update is called once per frame
	void Update () {

        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0.0f){
            Destroy(this.gameObject);
        }


	}

    internal void setDirection(Vector2 bulletDirection) {
        this.bulletDirection = bulletDirection;
    }

	private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Obstacles"){
            Destroy(gameObject);
        }
	}
}
