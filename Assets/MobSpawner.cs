﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MobSpawner : MonoBehaviour {
    
    public Grid gameGrid;
    public Tile startTile;
    public Tilemap island;
    private IslandBuilder islandBuilder;

    public GameObject gameObjectToSpawn;
    public GameObject gameObjectToSpawn2;


    public int minTimeBetweenSpawns = 3;
    public int maxTimeBetweenSpawns = 5;

    public float timeToNextSpawn = 0.0f;

    public List<GameObject> pickups = new List<GameObject>();


    List<Vector3Int> possibleSpawnPoints = new List<Vector3Int>();

	// Use this for initialization
	void Start () {
        this.islandBuilder = gameGrid.GetComponent<IslandBuilder>();
        calculateNextSpawn();
	}

    private void calculateNextSpawn(){
        timeToNextSpawn = UnityEngine.Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns);
    }
	
	// Update is called once per frame
	void Update () {

        timeToNextSpawn -= Time.deltaTime;

        if (timeToNextSpawn <= 0.0f) {

            if (islandBuilder.initialGenerationDone) {
                for (int x = 0; x < 100; x++) {
                    for (int y = 0; y < 100; y++) {
                        Vector3Int v = new Vector3Int(x, y, 0);
                        if (island.HasTile(v)) {
                            if (island.GetTile(v).Equals(startTile)) {
                                possibleSpawnPoints.Add(v);
                            }
                        }

                    }
                }

                int randomPosition = UnityEngine.Random.Range(0, possibleSpawnPoints.Count);

                Vector3Int position = possibleSpawnPoints[randomPosition];
                if (UnityEngine.Random.Range(0, 10) == 0) {
                    spawnPickup(position.x + 1, position.y + 1);
                }
                else {
                    spawnMonster(position.x + 1, position.y + 1);
                }
            }

            calculateNextSpawn();
        }

	}

    private void spawnMonster(int x, int y) {
        Vector3 newMobPosition = new Vector3(x, y, 1);
        GameObject mob = null;
        if (UnityEngine.Random.Range(0, 2) == 0) {
            mob = Instantiate(gameObjectToSpawn2);
        }
        else {
            mob = Instantiate(gameObjectToSpawn);
        }
        mob.transform.position = newMobPosition;

    }

    private void spawnPickup(int x, int y) {

        if (pickups.Count > 0) {
            int pickupIndex = UnityEngine.Random.Range(0, pickups.Count);
            GameObject pickup = pickups[pickupIndex];

            Vector3 newPickupPosition = new Vector3(x, y, 1);
            Instantiate(pickup, newPickupPosition, Quaternion.identity);

            Debug.Log("Pickup spawned!");
        }
    }
}
