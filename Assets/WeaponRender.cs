﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRender : MonoBehaviour {


    public GameObject bazooka;
    public GameObject gun;
    public GameObject rifle;
    private SpriteRenderer bazookaSpriteRenderer;
    private SpriteRenderer gunSpriteRenderer;
    private SpriteRenderer rifleSpriteRenderer;
    public Rigidbody2D playerBody;
    private bool shooting;
    private ShootBehaviour.SelectedWeapon previousSelectedWeapon;

    // Use this for initialization
	void Start () {
        this.bazookaSpriteRenderer = bazooka.GetComponent<SpriteRenderer>();
        this.gunSpriteRenderer = gun.GetComponent<SpriteRenderer>();
        this.rifleSpriteRenderer = rifle.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (gun.activeSelf){
            renderGun();
        }

        if (bazooka.activeSelf) {
            renderBazooka();
        }

        if (rifle.activeSelf){
            renderRifle();
        }
    }

    private void renderRifle(){
        if (shooting) {
            this.rifleSpriteRenderer.sortingLayerName = "Effects";
            this.rifle.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            this.rifle.transform.localPosition = new Vector3(this.rifle.transform.localPosition.x, -0.01f, 0);

            if (this.playerBody.velocity.x < 0.0f) {
                this.rifle.transform.localPosition = new Vector3(-0.15f, -0.01f, 0);
                this.rifleSpriteRenderer.flipX = true;
            }
            else if (this.playerBody.velocity.x > 0.0f) {
                this.rifle.transform.localPosition = new Vector3(0.15f, -0.01f, 0);
                this.rifleSpriteRenderer.flipX = false;
            }
        }
        else {

            if (this.playerBody.velocity.x > 0.0f) {
                this.rifle.transform.eulerAngles = new Vector3(0f, 0f, 40f);
                this.rifle.transform.localPosition = new Vector3(0.12f, 0.124f, 0);
                this.rifleSpriteRenderer.sortingLayerName = "Entities";
                this.rifleSpriteRenderer.flipX = false;
            }
            else if (this.playerBody.velocity.x < 0.0f) {
                this.rifle.transform.eulerAngles = new Vector3(0f, 0f, -40f);
                this.rifle.transform.localPosition = new Vector3(-0.12f, 0.124f, 0);
                this.rifleSpriteRenderer.sortingLayerName = "Entities";
                this.rifleSpriteRenderer.flipX = true;
            }

            if (this.playerBody.velocity.y > 0) {

                this.rifleSpriteRenderer.sortingLayerName = "Effects";
                if (this.playerBody.velocity.x < 0.0f) {
                    this.rifleSpriteRenderer.flipX = true;
                }
            }
            else {
                this.rifleSpriteRenderer.sortingLayerName = "Entities";
            }
        }
    }

    internal void setSelectedWeapon(ShootBehaviour.SelectedWeapon selectedWeapon) {

        if (this.previousSelectedWeapon != selectedWeapon) {

            switch (selectedWeapon) {
                case ShootBehaviour.SelectedWeapon.gun:
                    gun.SetActive(true);
                    rifle.SetActive(false);
                    bazooka.SetActive(false);
                    break;
                case ShootBehaviour.SelectedWeapon.rifle:
                    gun.SetActive(false);
                    rifle.SetActive(true);
                    bazooka.SetActive(false);
                    break;
                case ShootBehaviour.SelectedWeapon.bazooka:
                    gun.SetActive(false);
                    rifle.SetActive(false);
                    bazooka.SetActive(true);
                    break;
            }

            this.previousSelectedWeapon = selectedWeapon;
        }

    }

    private void renderGun(){

        if (shooting){
            this.gunSpriteRenderer.sortingLayerName = "Effects";
            if (this.playerBody.velocity.x < 0.0f) {
                this.gun.transform.eulerAngles = new Vector3(0f, 0f, 0f);
                this.gun.transform.localPosition = new Vector3(-0.26f, -0.09f, 0);
                this.gunSpriteRenderer.flipX = true;
                this.gunSpriteRenderer.flipY = false;
            }
            else if (this.playerBody.velocity.x > 0.0f) {
                this.gun.transform.eulerAngles = new Vector3(0f, 0f, 0f);
                this.gun.transform.localPosition = new Vector3(0.26f, -0.09f, 0);
                this.gunSpriteRenderer.flipX = false;
                this.gunSpriteRenderer.flipY = false;
            }
        }else{
            if (this.playerBody.velocity.x > 0.0f) {
                this.gun.transform.eulerAngles = new Vector3(0f, 0f, 90f);
                this.gun.transform.localPosition = new Vector3(0.32f, -0.23f, 0);
                this.gunSpriteRenderer.sortingLayerName = "Effects";
                this.gunSpriteRenderer.flipX = true;
                this.gunSpriteRenderer.flipY = true;
            }
            else if (this.playerBody.velocity.x < 0.0f) {
                this.gun.transform.eulerAngles = new Vector3(0f, 0f, 90f);
                this.gun.transform.localPosition = new Vector3(-0.32f, -0.23f, 0);
                this.gunSpriteRenderer.sortingLayerName = "Effects";
                this.gunSpriteRenderer.flipX = true;
                this.gunSpriteRenderer.flipY = false;
            }
        }

    }

    private void renderBazooka() {
        if (shooting) {
            this.bazookaSpriteRenderer.sortingLayerName = "Effects";
            this.bazooka.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            this.bazooka.transform.localPosition = new Vector3(this.bazooka.transform.localPosition.x, -0.05f, 0);

            if (this.playerBody.velocity.x < 0.0f) {
                this.bazooka.transform.localPosition = new Vector3(-0.15f, -0.05f, 0);
                this.bazookaSpriteRenderer.flipX = true;
            }
            else if (this.playerBody.velocity.x > 0.0f) {
                this.bazooka.transform.localPosition = new Vector3(0.15f, -0.05f, 0);
                this.bazookaSpriteRenderer.flipX = false;
            }
        }
        else {

            if (this.playerBody.velocity.x > 0.0f) {
                this.bazooka.transform.eulerAngles = new Vector3(0f, 0f, 40f);
                this.bazooka.transform.localPosition = new Vector3(0.12f, 0.124f, 0);
                this.bazookaSpriteRenderer.sortingLayerName = "Entities";
                this.bazookaSpriteRenderer.flipX = false;
            }
            else if (this.playerBody.velocity.x < 0.0f) {
                this.bazooka.transform.eulerAngles = new Vector3(0f, 0f, -40f);
                this.bazooka.transform.localPosition = new Vector3(-0.12f, 0.124f, 0);
                this.bazookaSpriteRenderer.sortingLayerName = "Entities";
                this.bazookaSpriteRenderer.flipX = true;
            }

            if (this.playerBody.velocity.y > 0) {

                this.bazookaSpriteRenderer.sortingLayerName = "Effects";
                if (this.playerBody.velocity.x < 0.0f) {
                    this.bazookaSpriteRenderer.flipX = true;
                }
            }
            else {
                this.bazookaSpriteRenderer.sortingLayerName = "Entities";
            }
        }
    }

    internal void isShooting(bool b) {
        this.shooting = b;
    }
}
