﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScene : MonoBehaviour {
    private bool endScenePlaying;
    private Rigidbody2D body;
    private Vector2 velocityVector;

    // Use this for initialization
	void Start () {
        this.body = GetComponent<Rigidbody2D>();
        this.velocityVector = new Vector2();
	}
	
	// Update is called once per frame
	void Update () {
        if (this.endScenePlaying){
            
        }
	}

    public void playEndScene(){
        this.endScenePlaying = true;
    }

    public bool isEndScenePlaying(){
        return this.endScenePlaying;
    }
}
