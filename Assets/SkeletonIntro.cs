﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonIntro : MonoBehaviour {

    private Rigidbody2D body;

    private float counter = 2.0f;
	// Use this for initialization
	void Start () {
        this.body = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {

        if (this.transform.position.x <= -2.4f) {
            this.body.velocity = new Vector3(0, 0);
        }else{
            this.body.velocity = new Vector3(-0.1f, -0.1f);
            transform.localScale += new Vector3(0.05f * Time.deltaTime, 0.05f * Time.deltaTime, 0);
        }


        counter -= Time.deltaTime;

        if (counter < 0.0f){
            if (Input.anyKey){
                UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
            }
        }


	}
}
