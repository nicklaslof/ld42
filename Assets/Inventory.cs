﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {


    public UnityEngine.UI.Image selection;

    public int selectedSlotIndex = 0;

    private Vector2 selectedPosition = new Vector2();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float mouseWheel = Input.GetAxisRaw("Mouse ScrollWheel");

        if (mouseWheel > 0f){
            this.selectedSlotIndex++;
        }

        if (mouseWheel < 0f){
            this.selectedSlotIndex--;
        }

        if (this.selectedSlotIndex > 2){
            this.selectedSlotIndex = 0;
        }

        if (this.selectedSlotIndex < 0){
            this.selectedSlotIndex = 2;
        }

        if (Input.GetKey("1")){
            this.selectedSlotIndex = 0;
        }else if (Input.GetKey("2")) {
            this.selectedSlotIndex = 1;
        }else if (Input.GetKey("3")) {
            this.selectedSlotIndex = 2;
        }
 

        float slotX = 0;

        switch(selectedSlotIndex){
            case 0:
                slotX = -108f;
                break;
            case 1:
                slotX = -54;
                break;
            case 2:
                slotX = 0f;
                break;
            //case 3:
            //    slotX = 54f;
            //    break;
            //case 4:
            //    slotX = 108f;
            //    break;
        }

        if (selection){
           // Debug.Log(selectedPosition);
            selectedPosition.x = slotX;
            selectedPosition.y = 12.5f;

            selection.transform.localPosition = selectedPosition;
        }
	}
}
