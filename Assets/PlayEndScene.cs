﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayEndScene : MonoBehaviour {

    private Rigidbody2D body;
    private Animator animator;
	// Use this for initialization
	void Start () {
        this.animator = GetComponent<Animator>();

        this.body = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {
        if (this.animator){
            this.animator.SetBool("InBoat", true);
        }

        this.body.velocity = new Vector2(0.3f, 0);
	}
}
