﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBehaviour : MonoBehaviour {
    private Rigidbody2D body;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
        this.animator = GetComponent<Animator>();
        this.body = GetComponent<Rigidbody2D>();
        this.spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (this.body.velocity.y > 0) {
            this.animator.SetBool("Up", true);
        }

        if (this.body.velocity.y < 0) {
            this.animator.SetBool("Up", false);
        }

        if (this.body.velocity.y == 0.0f && Math.Abs(this.body.velocity.x) > 0.0f) {
            this.animator.SetBool("Up", false);
        }

        if (this.body.velocity.x > 0.0f) {
            this.spriteRenderer.flipX = false;
        }
        else if (this.body.velocity.x < 0.0f) {
            this.spriteRenderer.flipX = true;
        }

        if (Math.Abs(this.body.velocity.x) > 0.0f) {
            this.animator.SetFloat("Speed", Math.Abs(this.body.velocity.x));
        }
        else if (Math.Abs(this.body.velocity.y) > 0.0f) {
            this.animator.SetFloat("Speed", Math.Abs(this.body.velocity.y));
        }
        else {
            this.animator.SetFloat("Speed", 0);
        }
	}


}
