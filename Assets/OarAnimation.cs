﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OarAnimation : MonoBehaviour {

    float counter;
    public float speed;

	// Use this for initialization
	void Start () {
  
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;

        float sin = Mathf.Sin(counter*6);
        //Debug.Log(sin);
        transform.Rotate(new Vector3(0, 0, sin*speed));


	}
}
