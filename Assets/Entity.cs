﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class Entity : MonoBehaviour {

    public bool isPlayer;

    public int maxHealth;
    public int health;
    public int poision;
    public bool spawnCircleOnDeath = true;
    public bool affectedByPoision = false;


    // The tag that will hurt this entity
    public string bulletHitTag;

    public GameObject spawnOnDeath;

    private float hitCountdown;
    private float poisionHitCountdown;
    private float poisionHealCountdown;

    private ParticleSystem ps;
    private ParticleSystem poisionps;
    private ParticleSystem healps;
    private ShootBehaviour shootBehaviour;
    public GameObject particleSystemGameObject;
    public GameObject poisionParticleSystemGameObject;
    public GameObject healParticleSystemGameObject;
    private bool inPoision;

    public List<GameObject> pickups = new List<GameObject>();
    public GameObject boat;
    public GameObject oar;

    private float healPoisionBonusCountdown;


    private AudioSource playerHurt;
    private AudioSource gunPickup;
    private AudioSource riflePickup;
    private AudioSource rocketPickup;
    private AudioSource boatPickup;

    private AudioSource rifle;

    private AudioSource skeletonHit;
    public Animator animatorObject;
    private AudioSource skeletonDead;

    private float fadeOutPlayerHurt;
    private bool resumePlayerHurt;
    static public int enemiesKilled;
    static  bool boatSpawned;
    static  bool oarSpawned;

    static bool hasBoat;
    static bool hasOar;

    public Tilemap water;

    public EndScene endScene;

    public Image statusText;

    public float introTextCountdown = 10f;

    // Use this for initialization
	void Start () {
        if (isPlayer) {
            enemiesKilled = 0;
            hasOar = false;
            hasBoat = false;
            boatSpawned = false;
            oarSpawned = false;
        }

        health = maxHealth;
        this.ps = particleSystemGameObject.GetComponent<ParticleSystem>();
        this.poisionps = poisionParticleSystemGameObject.GetComponent<ParticleSystem>();
        this.healps = healParticleSystemGameObject.GetComponent<ParticleSystem>();
        this.shootBehaviour = GetComponent<ShootBehaviour>();
        this.playerHurt = GameObject.Find("PlayerHurt").GetComponent<AudioSource>();
        this.gunPickup = GameObject.Find("Pickup-Gun").GetComponent<AudioSource>();
        this.riflePickup = GameObject.Find("Pickup-Rifle").GetComponent<AudioSource>();
        this.rocketPickup = GameObject.Find("Pickup-Rocket").GetComponent<AudioSource>();
        this.boatPickup = GameObject.Find("Pickup-Boat").GetComponent<AudioSource>();
        if (GameObject.Find("SkeletonDead")) {
            this.skeletonDead = GameObject.Find("SkeletonDead").GetComponent<AudioSource>();
        }

        if (GameObject.Find("SkeletonHurt")){
            this.skeletonHit = GameObject.Find("SkeletonHurt").GetComponent<AudioSource>();
        }





        //Debug.Log(this.ps);
	}

    // Update is called once per frame
    void Update() {

        if (isPlayer) {
            if (introTextCountdown > 9) {
                statusText.GetComponent<FontRender>().text = "SHOOT SKELETONS AND FIND THE BOAT SO WE CAN LEAVE THIS HAUNTED ISLAND!";
            }

            if (introTextCountdown > 0) {
                introTextCountdown -= Time.deltaTime;
            }

            if (introTextCountdown < 0) {
                statusText.GetComponent<FontRender>().text = "";
                introTextCountdown = 0.0f;
            }

            if (hasBoat && introTextCountdown <= 0.0f){
                statusText.GetComponent<FontRender>().text = "WE HAVE THE BOAT NOW LETS FIND THE OAR SO WE CAN LEAVE THE ISLAND!";
                introTextCountdown = 8;
            }

            if (hasOar && hasBoat){
                statusText.GetComponent<FontRender>().text = "WE HAVE THE BOAT AND THE OAR LETS GO TO THE WATER AND LEAVE THE ISLAND!";
            }




        }

        if (health <= 0) {
            spawnCircle();

            if (!affectedByPoision) { //ugly detection that entity isnt the player
                enemiesKilled++;
                if (skeletonDead) {
                    skeletonDead.Play();

                }
               // Debug.Log("Playing audio");
            }

            spawnPickup();

            if (isPlayer){
                UnityEngine.SceneManagement.SceneManager.LoadScene("GameOver");
            }

            Destroy(this.gameObject);

        }

        if (hitCountdown > 0.0f) {
            hitCountdown -= Time.deltaTime / 2f;
        }

        if (hitCountdown <= 0.0f) {
            hitCountdown = 0.0f;
        }

        if (playerHurt) { 
            

            if (resumePlayerHurt){
                if (!gunPickup.isPlaying && ! riflePickup.isPlaying && !rocketPickup.isPlaying){
                    resumePlayerHurt = false;
                  //  Debug.Log("Resuming Player hurt");
                    playerHurt.volume = 1.0f;
                }
            }else{
                if (fadeOutPlayerHurt > 0) {
                    fadeOutPlayerHurt -= Time.deltaTime;
                }

                if (fadeOutPlayerHurt > 0 && playerHurt.isPlaying) {
                    playerHurt.volume = fadeOutPlayerHurt;
                }
            }

        }



        if (affectedByPoision){
            if (inPoision){
                poisionHitCountdown -= Time.deltaTime;

                if (poisionHitCountdown <= 0.0f) {
                    poision++;
                    if (!playerHurt.isPlaying) {
                       // Debug.Log("Playing hurt");
                        fadeOutPlayerHurt = 0.0f;
                        playerHurt.volume = 1.0f;
                        playerHurt.Play(0);
                    }
                    if (poision < 99) {
                        if (poisionps) {
                            if (poisionps.isStopped) {
                                poisionps.Clear();
                                poisionps.Play();
                            }
                        }
                    }

                    poisionHitCountdown = 0.025f;
                }
            }else{
                poisionHealCountdown -= Time.deltaTime;

                if (poisionHealCountdown <= 0.0f) {
                    poision--;
                    if (fadeOutPlayerHurt == 0.0f){
                        fadeOutPlayerHurt = 1.0f;
                    }

                    if (fadeOutPlayerHurt < 0.1f && playerHurt.isPlaying) {
                        playerHurt.Stop();
                    }
                    //if (playerHurt.isPlaying) {
                    //    playerHurt.Stop();
                    //}
                    if (healPoisionBonusCountdown > 0.0f){
                        poisionHealCountdown = 0.05f;
                    }else{
                        poisionHealCountdown = 1.2f;
                    }

                }
            }

            if (poision > 99) {
                poision = 99;
                hit(1, 1.5f);
            }

            if (poision < 0) {
                poision = 0;
            }

            if (healPoisionBonusCountdown > 0.0f) {
                healPoisionBonusCountdown -= Time.deltaTime;
            }

            if (healPoisionBonusCountdown <= 0.0f) {
                healPoisionBonusCountdown = 0.0f;
            }



        }


	}

    private void playAudioIfNotAlreadyPlaying(AudioSource audioSource){
        if (!audioSource.isPlaying) {
            audioSource.Play();
            if (playerHurt) {
                if (playerHurt.isPlaying) {
                    playerHurt.volume = 0.0f;
                    //Debug.Log("Pausing playerhurt");
                    this.resumePlayerHurt = true;
                }
            }
        }
    }

	private void OnCollisionEnter2D(Collision2D collision) {
        string hitByTag = collision.gameObject.tag;


        if (isPlayer) {

            if (hitByTag.Equals("Water")){
                if (hasOar && hasBoat) {
                    //water.GetComponent<TilemapCollider2D>().enabled = false;
                    //water.GetComponent<CompositeCollider2D>().enabled = false;
                    //animatorObject.SetBool("InBoat", true);
                    UnityEngine.SceneManagement.SceneManager.LoadScene("EndScene");
                }

            }

            if (hitByTag.Equals("HealthPickup")) {
                heal(1);
                if (healps.isStopped) {
                    healps.Clear();
                    healps.Play();
                }
                Destroy(collision.gameObject);
            }

            if (hitByTag.Equals("GunbulletPickup")) {
                if (shootBehaviour) {
                    playAudioIfNotAlreadyPlaying(gunPickup);
                    shootBehaviour.fillBullets(ShootBehaviour.SelectedWeapon.gun, 15);
                }
                Destroy(collision.gameObject);
            }

            if (hitByTag.Equals("RiflebulletPickup")) {
                if (shootBehaviour) {
                    playAudioIfNotAlreadyPlaying(riflePickup);
                    shootBehaviour.fillBullets(ShootBehaviour.SelectedWeapon.rifle, 10);
                }
                Destroy(collision.gameObject);
            }

            if (hitByTag.Equals("RocketPickup")) {
                if (shootBehaviour) {
                    playAudioIfNotAlreadyPlaying(rocketPickup);
                    shootBehaviour.fillBullets(ShootBehaviour.SelectedWeapon.bazooka, 1);
                }
                Destroy(collision.gameObject);
            }

            if (hitByTag.Equals("BoatPickup")) {
                if (shootBehaviour) {
                    shootBehaviour.foundBoat();
                    playAudioIfNotAlreadyPlaying(boatPickup);
                    Destroy(collision.gameObject);
                    hasBoat = true;
                }
                else {
                   // Debug.Log("NO STATS CONTROLLER!");
                }
            }

            if (hitByTag.Equals("OarPickup")) {
                if (shootBehaviour) {
                    shootBehaviour.foundOar();
                    Destroy(collision.gameObject);
                    hasOar = true;
                }
                else {
                  //  Debug.Log("NO STATS CONTROLLER!");
                }
            }

        }
        else {


            // Debug.Log(collision.gameObject + " " + hitByTag);
            if (hitByTag.Equals("GunBullet")) {
                hit(2, 0.2f);
                Destroy(collision.gameObject);
            }

            if (hitByTag.Equals("RifleBullet")) {
                hit(1, 0.1f);
                Destroy(collision.gameObject);
            }


            if (hitByTag.Equals("RocketBullet")) {
                hit(5, 0.3f);
                //Destroy(collision.gameObject);
            }
        }




	}

	private void OnTriggerStay2D(Collider2D collision) {
        string tag = collision.gameObject.tag;
//        Debug.Log(tag);
        if ((tag.Equals("CirclePoision") || tag.Equals("Skeleton") || tag.Equals("Bat")) && affectedByPoision) {
            //hit(1, 1.5f);
            this.inPoision = true;
        }
	}

	private void OnTriggerExit2D(Collider2D collision) {
        string tag = collision.gameObject.tag;
        if ((tag.Equals("CirclePoision") || tag.Equals("Skeleton") || tag.Equals("Bat")) && affectedByPoision) {
            //hit(1, 1.5f);
            this.inPoision = false;
        }
	}

    public void heal(int ammount){
        health += ammount;

        if (health > maxHealth){
            health = maxHealth;
        }

        this.healPoisionBonusCountdown = 4.0f;

    }

	public void hit(int ammount, float hitCooldown){
        if (hitCountdown > 0.0f){
            return;
        }

        health -=ammount;
        hitCountdown = hitCooldown;
        if (ps) {
            ps.Clear();
            ps.Play();
        }


        if (!affectedByPoision){ //ugly detection that entity isnt the player
            if (skeletonHit) {
                if (skeletonHit.isPlaying) {
                    skeletonHit.Stop();
                }
                skeletonHit.Play();
            }
        }

    }


	void spawnCircle() {

        if (spawnCircleOnDeath) {
            Vector3 circlePosition = new Vector3(transform.position.x, transform.position.y, 1);
            Instantiate(spawnOnDeath, circlePosition, Quaternion.identity);
        }
    }

    void spawnPickup(){

        bool specialsSpawned = false;
        Debug.Log("Skeltons killed "+ enemiesKilled +"  hasboat "+hasBoat+"   hasoar "+hasOar+"    boatSpawned "+boatSpawned+"   oarSpawned "+oarSpawned);

        if (enemiesKilled> 25){
            if (tag.Equals("Skeleton")) {
                int random = Random.Range(0, 50);
                if (random < enemiesKilled) {
                    if (!boatSpawned) {
                        Instantiate(boat, new Vector3(transform.position.x, transform.position.y, 1), Quaternion.identity);
                        boatSpawned = true;
                        specialsSpawned = true;
                    }
                    else if (!oarSpawned) {
                        Instantiate(oar, new Vector3(transform.position.x, transform.position.y, 1), Quaternion.identity);
                        oarSpawned = true;
                        specialsSpawned = true;
                    }
                }
            }
        }

        if (!specialsSpawned) {
            if (Random.Range(0, 2) == 0) {
                if (pickups.Count > 0) {
                    int pickupIndex = Random.Range(0, pickups.Count);
                    GameObject pickup = pickups[pickupIndex];

                    Instantiate(pickup, new Vector3(transform.position.x, transform.position.y, 1), Quaternion.identity);
                }
            }
        }
    }


}
