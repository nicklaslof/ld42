﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FontRender : MonoBehaviour {
    private Image img;
    private Texture2D texture;
    private Texture2D fontTexture;
    public Sprite fontSprite;

    private string fontCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!";
    public string text;

    // Use this for initialization
	void Start () {
        this.img = GetComponent<Image>();
        //this.img.color = Color.white;
        this.fontTexture = fontSprite.texture;
        this.texture = new Texture2D((int)this.img.rectTransform.rect.width, (int)this.img.rectTransform.rect.height);
        this.texture.filterMode = FilterMode.Point;
        this.img.material.mainTexture = texture;

	}
	
	// Update is called once per frame
	void Update () {

        //Debug.Log(text);

        for (int x = 0; x < texture.width; x++) {
            for (int y = 0; y < texture.height; y++) {
                this.texture.SetPixel(x, y, new Color(0,0,0,0));
            }
        }

        int destX = 0;
        for (int i = 0; i < text.Length; i++) {
            char c = text[i];
            int fontIndex = fontCharacters.IndexOf(c);
            for (int x = fontIndex*6; x < (fontIndex*6)+6; x++) {
                for (int y = 0; y < 8; y++) {
                    Color color = this.fontTexture.GetPixel(x, y);
                    this.texture.SetPixel(destX, y, color);
                }
                destX++;
            }
        }
        this.texture.Apply();
	}
}
