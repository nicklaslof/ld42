﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBehaviour : MonoBehaviour {

    public GameObject gunBullet;
    public GameObject rifleBullet;
    public GameObject rocketBullet;

    private float shootLimitCounter;

    private WeaponRender weaponRender;
    private StatsPanelController statsPanelController;

    public SelectedWeapon selectedWeapon;

    private AudioSource rifle;
    private AudioSource gun;
    private AudioSource outOfAmmo;

    public Dictionary<SelectedWeapon, int> bullets = new Dictionary<SelectedWeapon, int>();

    public enum SelectedWeapon{
        gun,
        rifle,
        bazooka
    }


    // Use this for initialization
	void Start () {
        this.weaponRender = GetComponent<WeaponRender>();
        this.statsPanelController = GetComponent<StatsPanelController>();

        bullets.Add(SelectedWeapon.gun, 25);
        bullets.Add(SelectedWeapon.rifle, 20);
        bullets.Add(SelectedWeapon.bazooka, 2);


        this.rifle = GameObject.Find("Rifle").GetComponent<AudioSource>();
        this.gun = GameObject.Find("GunSound").GetComponent<AudioSource>();
        this.outOfAmmo = GameObject.Find("OutofAmmo").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {


        int selectedWeaponIndex = this.statsPanelController.selectedSlotIndex;

        switch(selectedWeaponIndex){
            case 0:
                selectedWeapon = SelectedWeapon.gun;
                break;
            case 1:
                selectedWeapon = SelectedWeapon.rifle;
                break;
            case 2:
                selectedWeapon = SelectedWeapon.bazooka;
                break;
        }

        weaponRender.setSelectedWeapon(selectedWeapon);


        int bulletsLeft = bullets[selectedWeapon];
        //bullets.TryGetValue(selectedWeapon,out bulletsLeft);
        this.statsPanelController.setBulletsLeft(bulletsLeft);


        shootLimitCounter -= Time.deltaTime;


		
        if (Input.GetAxisRaw("Fire1") > 0.0f){

            weaponRender.isShooting(true);

            if (shootLimitCounter > 0.0f) {
               
                return;
            }

            if (bulletsLeft < 1) {
                this.outOfAmmo.Play();
                switch (selectedWeapon) {
                    case SelectedWeapon.gun: {
                        shootLimitCounter = 1.0f;
                        break;
                        }
                    case SelectedWeapon.rifle: {
                        shootLimitCounter = 0.4f;
                        break;
                        }
                    case SelectedWeapon.bazooka: {
                        shootLimitCounter = 3f;
                        break;
                        }
                }
                return;
            }


            if (Camera.current == null){
                return;
            }

            Vector3 bulletLocation = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            Vector3 worldInputPos = Camera.current.ScreenToWorldPoint(Input.mousePosition);
            worldInputPos.z = 0.0f;



            Vector2 bulletDirection = worldInputPos - new Vector3(transform.position.x, transform.position.y, 0);

            /*if (bulletDirection.x > 0.0f){
                bulletLocation.x += 0.3f;
            }

            if (bulletDirection.x < 0.0f) {
                bulletLocation.x -= 0.3f;
            }*/

            //if (bulletDirection.y > 0.0f) {
           //     bulletLocation.y += 0.4f;
           // }

          //  if (bulletDirection.y < 0.0f) {
         //       bulletLocation.y -= 0.4f;
         //   }



            bulletDirection.Normalize();

            GameObject target = null;
            Quaternion quaternion = Quaternion.identity;

            switch(selectedWeapon){
                case SelectedWeapon.gun: {
                        shootLimitCounter = 1.0f;
                        float rotation = Mathf.Atan2(-bulletDirection.x, -bulletDirection.y);
                        float value = (rotation * (180f / 3.1415927f)) + 90;
                        quaternion = Quaternion.Euler(0, 0, -value);
                        target = Instantiate(gunBullet, bulletLocation, quaternion);
                        this.gun.Play();
                        break;
                    }
                case SelectedWeapon.rifle: {
                        shootLimitCounter = 0.4f;
                        float rotation = Mathf.Atan2(-bulletDirection.x, -bulletDirection.y);
                        float value = (rotation * (180f / 3.1415927f)) + 90;
                        quaternion = Quaternion.Euler(0, 0, -value);
                        target = Instantiate(rifleBullet, bulletLocation, quaternion);
                        this.rifle.Play();
                        break;
                    }
                case SelectedWeapon.bazooka: {

                        float rotation = Mathf.Atan2(-bulletDirection.x, -bulletDirection.y);
                        float value = (rotation * (180f / 3.1415927f)) + 90;
                        quaternion = Quaternion.Euler(0, 0, -value);

                        target = Instantiate(rocketBullet, bulletLocation, quaternion);

                        shootLimitCounter = 3f;
                        break;
                    }
            }

            BulletBehaviour bulletBehaviour = target.GetComponent<BulletBehaviour>();
            if (bulletBehaviour){
                bulletBehaviour.setDirection(bulletDirection);
            }

            int newBulletsLeft = bulletsLeft - 1;
            if (newBulletsLeft < 0 ){
                newBulletsLeft = 0;
            }

            bullets[selectedWeapon] = newBulletsLeft;


        }else{
            weaponRender.isShooting(false);
        }

	}

    public void fillBullets(SelectedWeapon selectedWeapon, int ammount){
        bullets[selectedWeapon] += ammount;
        if (bullets[selectedWeapon] > 99){
            bullets[selectedWeapon] = 99;
        }
    }

    internal void foundBoat() {
        statsPanelController.foundBoat();
    }

    internal void foundOar() {
        statsPanelController.foundOar();
    }
}
