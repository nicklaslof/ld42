﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject targetToFollow;

    private Vector3 cameraPosition = new Vector3();
    private Vector3 sizeDst = new Vector3(6, 0, 0);

    public float smoothTime = 0.25f;

    Vector3 currentVelocity;
    Vector3 currentVelocity2;


    public Texture2D texture;
    private Camera ortoCamera;

    // Use this for initialization
	void Start () {


        Cursor.SetCursor(texture, new Vector2(8, 8), CursorMode.ForceSoftware);
        this.ortoCamera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (targetToFollow != null){


            if (ortoCamera.orthographicSize > sizeDst.x) {
                Vector3 sizeSrc = new Vector3(ortoCamera.orthographicSize, 0, 0);
                ortoCamera.orthographicSize = Vector3.SmoothDamp(sizeSrc, sizeDst, ref currentVelocity2, smoothTime).x;
            }


            Vector3 pos = Vector3.SmoothDamp(this.transform.position, targetToFollow.transform.position, ref currentVelocity, smoothTime);
            pos.z = -10f;

            this.transform.position = pos;

            //this.transform.position = cameraPosition;

        }
	}
}
