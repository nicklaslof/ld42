﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleBehaviour : MonoBehaviour {


    private Vector3 newScale = new Vector3();
    private float scaleCounter;
    public float scaleModifier = 0.1f;


    public Light spotlight;
    private SpriteRenderer spriteRenderer;

    private Color spriteColor;
    private bool finishedExpansion;
    private bool removeGameObject;

    // Use this for initialization
	void Start () {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.spriteColor = spriteRenderer.color;
	}
	
	// Update is called once per frame
	void Update () {

        if (!finishedExpansion) {
            scaleCounter += scaleModifier * Time.deltaTime;
            scaleModifier -= Time.deltaTime / 1000f;


            float f = Mathf.Lerp(1f, 5f, scaleCounter);
            float f2 = Mathf.Lerp(26f, 200f, scaleCounter);

            //Debug.Log(scaleModifier);
            newScale.x = f;
            newScale.y = f;
            newScale.z = transform.localScale.z;

            transform.localScale = newScale;


            /*spotlight.spotAngle = f2;
            spotlight.intensity = spotlight.intensity - (Time.deltaTime / 50f);*/
            if (f >= 5) {
                finishedExpansion = true;
            }
        }


        if (finishedExpansion) {
            this.finishedExpansion = true;

            this.spriteColor.a -= Time.deltaTime / 200f;

            this.spriteRenderer.color = this.spriteColor;

           // spotlight.intensity = spotlight.intensity - (Time.deltaTime / 50f);

            if (this.spriteRenderer.color.a <= 0.1f){
                this.removeGameObject = true;
            }
        }

        if (this.removeGameObject){
            Destroy(this.gameObject);
        }
	}
}
