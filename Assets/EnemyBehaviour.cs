﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {
    private Rigidbody2D body;

    public float speed = 1.5f;

    private GameObject target;



    private Vector3 velocity;

    // Use this for initialization
	void Start () {
        this.body = GetComponent<Rigidbody2D>();
        this.target = GameObject.Find("Player");
	}

	private void FixedUpdate() {
        Vector3 targetPosition = target.transform.position;

        velocity = targetPosition - transform.position;

        velocity.Normalize();

        body.velocity = velocity * speed;

	}

 

	void Update () {

	}

}
