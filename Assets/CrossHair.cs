﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHair : MonoBehaviour {

    Vector3 pos;

    public Transform anchor;
    public float maxMoveRadius;
    public float sensitivity = 1f;

	// Use this for initialization
	void Start () {
		
	}


	private void FixedUpdate() {
        transform.position = pos;
	}

	// Update is called once per frame
	void LateUpdate () {
        if (Camera.current != null) {
            Vector3 worldInputPos = Camera.current.ScreenToWorldPoint(Input.mousePosition);
            worldInputPos.z = 0.0f;

            pos = worldInputPos;
        }

       
        //transform.position = pos;


	}
}
