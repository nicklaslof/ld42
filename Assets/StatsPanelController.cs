﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsPanelController : MonoBehaviour {



    public Image health;
    public Image bullet;
    public Image poision;
    public Image weaponText;

    public GameObject boatIcon;
    public GameObject oarIcon;

    public GameObject inventoryPanel;


    private Entity entity;
    private FontRender healthRender;
    private FontRender bulletRender;
    private FontRender poisionRender;
    private FontRender weaponTextRender;
    private Inventory inventory;

    public int selectedSlotIndex;
    private int bulletsLeft;

    // Use this for initialization
	void Start () {
        this.entity = GetComponent<Entity>();
        this.healthRender = health.GetComponent<FontRender>();
        this.bulletRender = bullet.GetComponent<FontRender>();
        this.poisionRender = poision.GetComponent<FontRender>();
        this.weaponTextRender = weaponText.GetComponent<FontRender>();
        this.inventory = inventoryPanel.GetComponent<Inventory>();


	}
	
	// Update is called once per frame
	void Update () {

        this.healthRender.text = entity.health.ToString();
        this.poisionRender.text = entity.poision.ToString();
        this.weaponTextRender.text = getSelectedWeaponName();
        this.bulletRender.text = bulletsLeft.ToString();
        this.selectedSlotIndex = inventory.selectedSlotIndex;

	}

    private string getSelectedWeaponName(){
        switch (selectedSlotIndex) {
            case 0:
                return "GUN";
            case 1:
                return "THOMPSON GUN";
            case 2:
                return "BAZOOKA";
        }

        return "";
    }

    internal void setBulletsLeft(int bulletsLeft) {
        this.bulletsLeft = bulletsLeft;
    }

    public void foundBoat(){
        boatIcon.SetActive(true);
    }

    public void foundOar(){
        oarIcon.SetActive(true);
    }
}
